<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\StripeChargeController;
use App\Http\Controllers\API\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
});

Route::controller(StripeChargeController::class)->group( function () {
   Route::post('registerCard', 'storeCard');
})->middleware(['auth:api']);

Route::controller(PaymentController::class)->group( function () {
    Route::post('createPayment', 'chargeCustomer');
})->middleware(['auth:api']);
