<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTokenRequest;
use App\Models\UserCardToken;
use Illuminate\Support\Facades\Auth;
use Stripe\StripeClient;

class StripeChargeController extends Controller
{
    protected $client;
    public function __construct()
    {
        $this->client = new StripeClient(env('STRIPE_SECRET'));
    }

    public function storeCard(CreateTokenRequest $request) {
        try {
            $customer = $this->client->customers->create();

            $token = $this->client->customers->createSource($customer->id,
                ['source' => $request->token]
            );

            UserCardToken::create([
                'customer_id' => $customer->id,
                'user_id' => Auth::user()->id,
                'token' => $token->id,
                'exp_month' => $token->exp_month,
                'exp_year' => $token->exp_year,
                'last_four' => $token->last4,
            ]);

            return response()->json(['message' => 'Card created successfully'], 200);

        } catch (\Exception $e) {
            return response()->json($e->getMessage());
        }
    }


}
