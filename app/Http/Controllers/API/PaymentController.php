<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChargeRequest;
use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use Stripe\StripeClient;

class PaymentController extends Controller
{
    protected $client;
    public function __construct()
    {
        $this->client = new StripeClient(env('STRIPE_SECRET'));
    }

    public function chargeCustomer(ChargeRequest $request) {
        try {
           $payment = $this->client->charges->create([
               'amount' => $request->amount,
               'source' => Auth::user()->token->token,
               'currency' => $request->currency,
               'customer' => Auth::user()->token->customer_id
            ]);
           Payment::create([
               'user_id' => Auth::user()->id,
               'amount' => $payment->amount,
               'balance_transaction' => $payment->balance_transaction,
               'currency' => $payment->currency,
               'paid' => $payment->paid
           ]);

           return response()->json(['message' => 'Successfully created payment'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
