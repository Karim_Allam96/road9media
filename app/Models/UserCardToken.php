<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCardToken extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'token','exp_month','exp_year','last_four', 'customer_id'];

    protected $table = 'stripe_user_cards';

    public static $rules = [
        'token' => 'required',
    ];

    public static function getValidationRules() {
        return self::$rules;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
