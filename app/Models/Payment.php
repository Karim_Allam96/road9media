<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
      'user_id',
      'amount',
      'balance_transaction',
      'currency',
      'paid'
    ];

    protected $table = 'payments';

    public static $rules = [
        'amount' => 'required',
        'currency' => 'required',
    ];

    public static function getValidationRules() {
        return self::$rules;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
